﻿using Application.Authorization;
using Application.DTOs.Responses;
using Application.Features.TaskFeatures.Commands;
using Application.Features.TaskFeatures.Queries;
using Domain.Enums;
using Helpers.CustomValidation;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using static Application.Features.TaskFeatures.Queries.GetAll;

namespace WebApi.Controllers.v1
{
    [Route("api/[controller]")]
    [ApiController]
    //[DRCAuthorize(DefaultRoles.SUPER_ADMIN, DefaultRoles.ADMIN)]
    public class TaskController : BaseApiController
    {
        [HttpPost]
        //[ServiceFilter(typeof(NullObjFilter))]
        public async Task<IActionResult> Create(TaskDTO task)
        {
            await Mediator.Send(new Create.Command { Task = task });
            return Ok();
        }

        [HttpPut("{id}")]
        //[ServiceFilter(typeof(NullObjFilter))]
        public async Task<IActionResult> Update(int id,FullTaskDTO taskDTO)
        {
            taskDTO.Id = id;
            await Mediator.Send(new Update.Command { Task=taskDTO });
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new Delete.Command { Id = id });
            return Ok();
        }

        [HttpGet("GetAll/{UserId}")]
        public async Task<IActionResult> GetAll(string UserId , [FromQuery] TaskParams FilterParams)
        {

            var tasks = await Mediator.Send(new GetAll.Query { UserId=new System.Guid(UserId), FilterParams=FilterParams });
            return Ok(tasks);

        }

        [HttpGet("GetArchivedTasks/{UserId}")]
        public async Task<IActionResult> GetArchivedTasks(string UserId, int PageNumber, int PageSize)
        {

            var tasks = await Mediator.Send(new GetArchivedTasks.Query { UserId = new System.Guid(UserId), PageSize = PageSize, PageNumber = PageNumber});
            return Ok(tasks);

        }

        [HttpPut("ArchiveTask/{id}")]
        public async Task<IActionResult> ArchiveTask(int id)
        {
            await Mediator.Send(new ArchiveTask.Command { TaskId=id });
            return Ok();
        }
        [HttpPut("ChangeTaskStatus/{id}/{status}")]
        public async Task<IActionResult> ChangeTaskStatus(int id,StatusEnum status)
        {
            await Mediator.Send(new ChangeTaskStatus.Command { TaskId = id, TaskStatus=status });
            return Ok();
        }
    }
}
