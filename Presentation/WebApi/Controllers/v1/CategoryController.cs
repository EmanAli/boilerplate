﻿using Application.Authorization;
using Application.Features.CategoryFeatures.Commands;
using Application.Features.CategoryFeatures.Queries;
using Helpers.CustomValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace WebApi.Controllers.v1
{
    [Route("api/[controller]")]
    [ApiController]
    //[DRCAuthorize(DefaultRoles.SUPER_ADMIN, DefaultRoles.ADMIN)]
    public class CategoryController : BaseApiController
    {
       
        [HttpPost]
        //[ServiceFilter(typeof(NullObjFilter))]
        public async Task<IActionResult> Create(Create.Command command)
        {
            
            await Mediator.Send(command);
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetAll.Query()));

        }

        [HttpPut("{id}")]
        //[ServiceFilter(typeof(NullObjFilter))]
        public async Task<IActionResult> Update(int id, Update.Command command)
        {
            command.Id = id;
           await Mediator.Send(command);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new Delete.Command {  Id = id });
            return Ok();
        }
    }
}
