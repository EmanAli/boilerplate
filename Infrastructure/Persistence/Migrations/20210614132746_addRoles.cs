﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class addRoles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "ConcurrencyStamp", "CreatedBy", "CreatedOn", "IsDeleted", "LastModifiedBy", "LastModifiedOn", "Name", "NormalizedName" },
                values: new object[] { new Guid("ae3108fb-d5a7-4698-8cf1-139cd3fa385c"), "acdbe308-292a-4fc8-b843-0ccbbb6a76c7", null, new DateTime(2021, 6, 14, 15, 27, 42, 469, DateTimeKind.Local).AddTicks(2426), false, null, null, "Admin", "ADMIN" });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "ConcurrencyStamp", "CreatedBy", "CreatedOn", "IsDeleted", "LastModifiedBy", "LastModifiedOn", "Name", "NormalizedName" },
                values: new object[] { new Guid("512f31fe-04d6-46ac-9c52-44e358cf84cc"), "bf8dd941-9085-421f-884f-27ca3ad67ea2", null, new DateTime(2021, 6, 14, 15, 27, 42, 472, DateTimeKind.Local).AddTicks(7663), false, null, null, "SuperAdmin", "SUPERADMIN" });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "ConcurrencyStamp", "CreatedBy", "CreatedOn", "IsDeleted", "LastModifiedBy", "LastModifiedOn", "Name", "NormalizedName" },
                values: new object[] { new Guid("9246189b-9267-4d5a-b2cf-d4a7a98fa83e"), "b548e22f-a41e-4785-8cf9-8f153722d00a", null, new DateTime(2021, 6, 14, 15, 27, 42, 472, DateTimeKind.Local).AddTicks(7731), false, null, null, "User", "SELLER" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: new Guid("512f31fe-04d6-46ac-9c52-44e358cf84cc"));

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: new Guid("9246189b-9267-4d5a-b2cf-d4a7a98fa83e"));

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: new Guid("ae3108fb-d5a7-4698-8cf1-139cd3fa385c"));
        }
    }
}
