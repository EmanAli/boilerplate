﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.EntitiesConfiguration
{
    public class AppRoleConfiguration : IEntityTypeConfiguration<AppRole>
    {
        public void Configure(EntityTypeBuilder<AppRole> builder)
        {
            builder.ToTable(name: "Roles");

            builder.Property(m => m.Id).HasMaxLength(85);
            builder.Property(m => m.NormalizedName).HasMaxLength(85);
            builder.Property(m => m.Name).HasMaxLength(85);

            builder.HasMany(e => e.UserRoles)
                .WithOne(e => e.Role)
                .HasForeignKey(ur => ur.RoleId)
                .IsRequired(false);

            builder.HasMany(e => e.RoleClaims)
                .WithOne(e => e.Role)
                .HasForeignKey(rc => rc.RoleId)
                .IsRequired(false);

            builder.HasData(
               new AppRole
               {
                   Id = Guid.NewGuid(),
            Name = "Admin",
                   NormalizedName = "ADMIN",
                   IsDeleted = false,
                   CreatedOn = DateTime.Now
               },
                 new AppRole
                 {
                     Id = Guid.NewGuid(),
                     Name = "SuperAdmin",
                     NormalizedName = "SUPERADMIN",
                     IsDeleted = false,
                     CreatedOn = DateTime.Now
                 },
                   new AppRole
                   {
                       Id = Guid.NewGuid(),
                       Name = "User",
                       NormalizedName = "SELLER",
                       IsDeleted = false,
                       CreatedOn = DateTime.Now
                   }
               ); ;

        }
    }
}
