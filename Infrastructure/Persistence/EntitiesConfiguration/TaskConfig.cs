﻿using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistence.EntitiesConfiguration
{
    public class TaskConfig : IEntityTypeConfiguration<Task>
    {
        public void Configure(EntityTypeBuilder<Task> builder)
        {
            builder.Property(a => a.Title).IsRequired().HasMaxLength(50);
            builder.Property(a => a.Status).IsRequired().HasDefaultValue(StatusEnum.InProgress);
            builder.Property(a => a.Priority).IsRequired();
            builder.Property(a => a.FinishDate).IsRequired();
            builder.HasOne(a => a.Category).WithMany(a=>a.Tasks).HasForeignKey(a=>a.CatId);
            builder.HasOne(a => a.User).WithMany(a => a.Tasks).HasForeignKey(a => a.UserId);
        }
    }
}
