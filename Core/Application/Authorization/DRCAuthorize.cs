﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Authorization
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class DRCAuthorize : AuthorizeAttribute
    {

        public DRCAuthorize(params object[] roles)
        {

            if (roles.Any(r => r.GetType().BaseType != typeof(Enum)))
            {
                throw new ArgumentException("roles");
            }
            this.Roles = string.Join(",", roles.Select(r => Enum.GetName(r.GetType(), r)));
        }

    }
   
}
