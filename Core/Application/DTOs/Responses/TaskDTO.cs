﻿using Domain.Enums;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.DTOs.Responses
{
  public  class TaskDTO
    {
        public string Title { get; set; }

        public StatusEnum Status { get; set; }

        public PriorityEnum Priority { get; set; }

        public DateTime DueDate { get; set; }

        public DateTime FinishDate { get; set; }

        public int CatId { get; set; }
        public Guid UserId { get; set; }
        public class Validator : AbstractValidator<TaskDTO>
        {
            public Validator()
            {
                RuleFor(a => a.Title).NotEmpty().NotNull().MinimumLength(4).MaximumLength(25);
                RuleFor(a => a.Status).NotEmpty().NotNull();
                RuleFor(a => a.Priority).NotEmpty().NotNull();
                RuleFor(a => a.FinishDate).NotEmpty().NotNull();
            }

        }
    }
    
}
