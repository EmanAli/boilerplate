﻿using Application.DTOs.Responses;
using AutoMapper;
using Domain.Entities;
using Helpers.Models;

namespace Utilities
{
    public class MapperConfig : Profile
    {
        public MapperConfig()
        {
            CreateMap<Category, CategoryDto>().ReverseMap();
            CreateMap<Task, TaskDTO>().ReverseMap();
            CreateMap<FullTaskDTO, TaskDTO>().ReverseMap();
            CreateMap<FullTaskDTO, Task>().ReverseMap();
            CreateMap<PaginatedResult<FullTaskDTO>, PaginatedResult<Task>>().ReverseMap();

        }
    }
}
