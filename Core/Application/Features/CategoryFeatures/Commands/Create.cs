﻿using Domain.Entities;
using FluentValidation;
using Helpers.Interfaces;
using Helpers.Resources;
using MediatR;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.CategoryFeatures.Commands
{
   public class Create
    {
        public class Command:IRequest<bool>
        {
            public string Name { get; set; }
        }

        public class Validator:AbstractValidator<Command>
        {
            public Validator(IStringLocalizer<SharedResource> localizer)
            {
                RuleFor(a => a.Name).NotEmpty().NotNull().MinimumLength(4).MaximumLength(25).
                    WithName(x => localizer[ResourceKeys.Name].Value);

            }

        }

        public class Handler : IRequestHandler<Command,bool>
        {
            private readonly IUnitOfWork _uow;

            public Handler(IUnitOfWork uow)
            {
                _uow = uow;
            }

            public async Task<bool> Handle(Command request, CancellationToken cancellationToken)
            {
                Category category = new Category() { Name = request.Name };
                _uow.Repository<Category>().Add(category);
                await _uow.CompleteAsync();
                return true;
            }

           
        }
    }
}
