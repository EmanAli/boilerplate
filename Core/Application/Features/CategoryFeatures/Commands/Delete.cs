﻿using Domain.Entities;
using Helpers.Constants;
using Helpers.Exceptions;
using Helpers.Interfaces;
using Helpers.Resources;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.CategoryFeatures.Commands
{
   public  class Delete 
    {
        public class Command:IRequest
        {
            public int Id { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly IUnitOfWork _uow;

            public Handler(IUnitOfWork uow)
            {
                _uow = uow;
            }
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                // find obj 
              Category cat=await  _uow.Repository<Category>().GetByIdAsync(request.Id);
                if (cat==null)
                {
                    throw new AppCustomException(ErrorStatusCodes.NotFound,
                        new List<Tuple<string, string>> { new Tuple<string, string>(ResourceKeys.Category, ResourceKeys.DataNotFound) });

                }
                else
                {
                    _uow.Repository<Category>().Remove(cat);
                   await _uow.CompleteAsync();
                    return Unit.Value;
                }

            }
        }
    }
}
