﻿using Application.DTOs;
using Application.DTOs.Responses;
using AutoMapper;
using Domain.Entities;
using Helpers.Interfaces;
using Helpers.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.CategoryFeatures.Queries
{
   public class GetAll
    {
        public class Query:IRequest<Result<IEnumerable<CategoryDto>>>
        {

        }
        public class handle : IRequestHandler<Query, Result<IEnumerable<CategoryDto>>>
        {
            private readonly IUnitOfWork _uow;
            private readonly IMapper _mapper;

            public handle(IUnitOfWork uow , IMapper mapper)
            {
                _uow = uow;
                _mapper = mapper;
            }

            public async Task<Result<IEnumerable<CategoryDto>>> Handle(Query request, CancellationToken cancellationToken)
            {
                var result = (await _uow.Repository<Category>().ListAsync()).Select(a => new CategoryDto
                {
                    id = a.Id,
                    name = a.Name
                  
                });
                var temp = new Result<IEnumerable<CategoryDto>>
                {
                    Data = result
                };
                return temp;
            }
        }
    }
}
