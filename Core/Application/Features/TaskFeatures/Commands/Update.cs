﻿using Application.DTOs.Responses;
using AutoMapper;
using Helpers.Constants;
using Helpers.Exceptions;
using Helpers.Interfaces;
using Helpers.Resources;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.TaskFeatures.Commands
{
  public  class Update
    {
        public class Command :IRequest
        {
            public FullTaskDTO Task { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly IUnitOfWork _uow;
            private readonly IMapper _mapper;

            public Handler(IUnitOfWork uow, IMapper mapper)
            {
                _uow = uow;
                _mapper = mapper;
            }
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var task = await _uow.Repository<Domain.Entities.Task>().GetByIdAsync(request.Task.Id);
                
                if (task == null)
                {
                    throw new AppCustomException(ErrorStatusCodes.NotFound,
                    new List<Tuple<string, string>> { new Tuple<string, string>(ResourceKeys.Task, ResourceKeys.DataNotFound) });

                }
                else if (task.Status == Domain.Enums.StatusEnum.InProgress)
                {
                    var taskObj = _mapper.Map<Domain.Entities.Task>(task);
                    _uow.Repository<Domain.Entities.Task>().Update(taskObj);
                    await _uow.CompleteAsync();
                    return Unit.Value;
                }
                else
                {
                    throw new AppCustomException(ErrorStatusCodes.Forbidden,
                      new List<Tuple<string, string>> { new Tuple<string, string>(ResourceKeys.Task, "Can't update this obj check status ") });
                }
            }
        }
    }
}
