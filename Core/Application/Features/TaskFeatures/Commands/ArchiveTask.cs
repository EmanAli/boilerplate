﻿using Domain.Enums;
using Helpers.Constants;
using Helpers.Exceptions;
using Helpers.Interfaces;
using Helpers.Resources;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.TaskFeatures.Commands
{
   public class ArchiveTask
    {
        public class Command:IRequest<bool>
        {
            public int TaskId { get; set; }
        }
        public class Handler : IRequestHandler<Command, bool>
        {
            private readonly IUnitOfWork _uow;

            public Handler(IUnitOfWork uow)
            {
                _uow = uow;
            }
            public async Task<bool> Handle(Command request, CancellationToken cancellationToken)
            {
                // get task 
               var taskObj= await _uow.Repository<Domain.Entities.Task>().GetByIdAsync(request.TaskId);
                if (taskObj == null)
                {
                    throw new AppCustomException(ErrorStatusCodes.NotFound,
                  new List<Tuple<string, string>> { new Tuple<string, string>(ResourceKeys.Task, ResourceKeys.DataNotFound) });
                    
                }
                else if ( taskObj.Status == StatusEnum.Done || taskObj.Status == StatusEnum.Overdue)
                {
                    taskObj.IsDeleted = true;
                    _uow.Repository<Domain.Entities.Task>().Update(taskObj);
                    return true;
                }
                else
                {
                    throw new AppCustomException(ErrorStatusCodes.Forbidden,
                     new List<Tuple<string, string>> { new Tuple<string, string>(ResourceKeys.Task, "Can't update this obj check status ") });
                   
                }
               
            }
        }
    }
}
