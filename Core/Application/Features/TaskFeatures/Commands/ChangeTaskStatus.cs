﻿using Domain.Enums;
using Helpers.Constants;
using Helpers.Exceptions;
using Helpers.Interfaces;
using Helpers.Resources;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.TaskFeatures.Commands
{
   public  class ChangeTaskStatus
    {
        public class Command:IRequest<bool>
        {
            public int TaskId { get; set; }
            public StatusEnum TaskStatus { get; set; }
        }

        private class Handler : IRequestHandler<Command, bool>
        {
            private readonly IUnitOfWork uow;

            public Handler(IUnitOfWork uow)
            {
                this.uow = uow;
            }
            public async Task<bool> Handle(Command request, CancellationToken cancellationToken)
            {
                // get task 
              var taskObj=await  this.uow.Repository<Domain.Entities.Task>().GetByIdAsync(request.TaskId);
                if (taskObj == null)
                {
                    throw new AppCustomException(ErrorStatusCodes.NotFound,
                  new List<Tuple<string, string>> { new Tuple<string, string>(ResourceKeys.Task, ResourceKeys.DataNotFound) });

                }
                else if (request.TaskStatus == StatusEnum.Done && taskObj.DueDate > DateTime.Now)
                {
                    taskObj.Status = StatusEnum.Done;
                
                }
                else if(request.TaskStatus == StatusEnum.Done && taskObj.DueDate < DateTime.Now)
                {
                    taskObj.Status = StatusEnum.Overdue;
                }
                else
                {
                    taskObj.Status = request.TaskStatus;
                }
                this.uow.Repository<Domain.Entities.Task>().Update(taskObj);
                return true;
            }
        }
    }
}
