﻿using Application.DTOs.Responses;
using AutoMapper;
using Helpers.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.TaskFeatures.Commands
{
  public  class Create
    {
        public class Command:IRequest
        {
            public TaskDTO Task { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly IUnitOfWork _uow;
            private readonly IMapper _mapper;

            public Handler(IUnitOfWork uow , IMapper mapper)
            {
                _uow = uow;
                _mapper = mapper;
            }
            async Task<Unit> IRequestHandler<Command, Unit>.Handle(Command request, CancellationToken cancellationToken)
            {
                try
                {
                var taskObj = _mapper.Map<Domain.Entities.Task>(request.Task);
                    // mappe dto to Task
                  
                _uow.Repository<Domain.Entities.Task>().Add(taskObj);
                await _uow.CompleteAsync();
               
                }
                catch (Exception ex)
                {

                   
                }
                return Unit.Value;
            }
        }
    }
}
