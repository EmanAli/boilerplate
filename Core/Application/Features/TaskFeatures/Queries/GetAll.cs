﻿
using Application.DTOs.Responses;
using AutoMapper;
using Domain.Enums;
using Helpers.Interfaces;
using Helpers.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.TaskFeatures.Queries
{
   public  class GetAll
    {
        public class Query : IRequest<PaginatedResult<FullTaskDTO>>
        {
            public Guid UserId { get; set; }
           

            public TaskParams FilterParams { get; set; }
        }

        public class Handler : IRequestHandler<Query, PaginatedResult<FullTaskDTO>>
        {
            private readonly IUnitOfWork _uow;
            private readonly IMapper _mapper;

            public Handler(IUnitOfWork uow , IMapper mapper )
            {
                _uow = uow;
                _mapper = mapper;
            }
            public async Task<PaginatedResult<FullTaskDTO>> Handle(Query request, CancellationToken cancellationToken)
            {
                IQueryable< Domain.Entities.Task> tasks = _uow.Repository<Domain.Entities.Task>()
                    .FindByCondition(a => !a.IsDeleted && a.UserId == request.UserId, trackChanges: false)
                        .OrderBy(a => a.Status).ThenBy(a => a.DueDate).ThenBy(a => a.Priority);

                //filteration
                if (!string.IsNullOrWhiteSpace(request.FilterParams.Title))
                {
                    tasks= tasks.Where(a => a.Title.Equals(request.FilterParams.Title));
                }
                if ( request.FilterParams.CatId != 0)
                {
                    tasks = tasks.Where(a => a.CatId == request.FilterParams.CatId);
                }
                if (request.FilterParams.Status !=null && request.FilterParams.Status.Count>0)
                {
                    tasks = tasks.Where(a => a.Status.Equals(request.FilterParams.Status.Any()));
                }
                if (request.FilterParams.Priorities != null && request.FilterParams.Priorities.Count > 0)
                {
                    tasks = tasks.Where(a => a.Priority.Equals(request.FilterParams.Priorities.Any()));
                }
                var res= _uow.Repository<Domain.Entities.Task>().PaginatedList(tasks, request.FilterParams.PageNumber, request.FilterParams.PageSize);
                //mapping 
              return new PaginatedResult<FullTaskDTO> { Meta= res.Meta, Data= _mapper.Map<IEnumerable<FullTaskDTO>>(res.Data) }; 
                
                
            }

           
        }

        public class TaskParams 
        {
            public int PageNumber { get; set; }

            public int PageSize { get; set; }

            public string Title { get; set; }

            public int CatId { get; set; }

            public List<StatusEnum> Status { get; set; }

            public List<PriorityEnum> Priorities { get; set; }
        }
    }
}
