﻿using Application.DTOs.Responses;
using AutoMapper;
using Helpers.Interfaces;
using Helpers.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.TaskFeatures.Queries
{
   public  class GetArchivedTasks
    {
        public class Query:IRequest<PaginatedResult<FullTaskDTO>>
        {
            public Guid UserId { get; set; }

            public int PageNumber { get; set; }

            public int PageSize { get; set; }
        }

        public class Handler : IRequestHandler<Query, PaginatedResult<FullTaskDTO>>
        {
            private readonly IUnitOfWork _uow;
            private readonly IMapper _mapper;

            public Handler(IUnitOfWork uow, IMapper mapper)
            {
                _uow = uow;
                _mapper = mapper;
            }
            public async Task<PaginatedResult<FullTaskDTO>> Handle(Query request, CancellationToken cancellationToken)
            {
                var query = _uow.Repository<Domain.Entities.Task>().FindByCondition(a => a.UserId==request.UserId && a.IsDeleted, trackChanges: false)
                      .OrderByDescending(a => a.FinishDate);
              var result=  _uow.Repository<Domain.Entities.Task>().PaginatedList(query, request.PageNumber, request.PageSize);

                return  _mapper.Map<PaginatedResult<FullTaskDTO>>(result);

                 
            }
        }
    }
}
