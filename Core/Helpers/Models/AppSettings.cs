﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpers.Models
{
    public class AppSettings
    {
        public EmailSettings EmailSettings { get; set; }
        public List<ApiClient> ApiClients { get; set; }
        public JWTSettings JWTSettings { get; set; }
        public string Api_URL { get; set; }
    }

    public class EmailSettings
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string FromEmail { get; set; }
        public string Password { get; set; }
    }

    public class ApiClient
    {
        public string Name { get; set; }
        public string ApiKey { get; set; }
    }

    public class JWTSettings
    {
        public string SecretHashKey { get; set; }
        public int DurationInMillisecond { get; set; }
    }
}
