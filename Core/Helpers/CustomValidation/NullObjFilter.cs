﻿using Helpers.Constants;
using Helpers.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpers.CustomValidation
{
   public class NullObjFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
           
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var action = context.RouteData.Values["action"];
            var controller = context.RouteData.Values["controller"];
            var obj = context.ActionArguments.SingleOrDefault().Value;
            if (obj == null)
            {
               
                throw new AppCustomException(ErrorStatusCodes.NoContent, 
                       new List<Tuple<string, string>> { new Tuple<string, string>("Object is null", $"Object is null. Controller:{controller} , action:{action}") });

                return;
            }
            if (!context.ModelState.IsValid)
            {
               
                context.Result = new UnprocessableEntityObjectResult(context.ModelState);
            }
        }
    }
}
