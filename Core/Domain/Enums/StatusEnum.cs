﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Enums
{
   public enum StatusEnum
    {
        InProgress=1,
        Done=2,
        Overdue=3
    }
}
