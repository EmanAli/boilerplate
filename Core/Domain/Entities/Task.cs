﻿using Domain.Common;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
   public class Task : Entity<int>
    {
        public string Title  { get; set; }

        public StatusEnum Status { get; set; }

        public PriorityEnum Priority { get; set; }

        public DateTime DueDate { get; set; }

        public DateTime FinishDate { get; set; }

        //[ForeignKey(nameof(Category))]
        public int CatId { get; set; }
        public virtual Category Category { get; set; }
        public  Guid UserId { get; set; }

        public virtual AppUser User { get; set; }
    }
}
